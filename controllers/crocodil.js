const CrocodilDb = require('../models').Crocodil;
const InsulaDb = require ("../models").Insula;


const controller = {
    getAllCrocodiles: async (req, res) => {
        CrocodilDb.findAll().then((crocodili) => {
            res.status(200).send(crocodili);
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error!"});
        }); 
    },

    addCrocodile: async (req, res) => {
        const { idInsula, nume, prenume, varsta } = req.body;
        InsulaDb.findByPk(idInsula)
        .then((insula) => {
            console.log(insula);
            if (insula) {
                insula
                .createCrocodil({nume, prenume, varsta})
                .then((crocodil) => {
                    res.status(201).send(crocodil);
                })
                .catch((err) => {
                    console.log(err);
                    res.status(500).send({message: "Server error"});
                });
            } else {
                res.status(404).send ({message: "Island not found"});
            }
        }).catch((err)=> {
            console.log (err);
            res.status(500).send({message: "Server error"});
        });
    },
    //stergerea crocodilului dupa id
    deleteCrocodile: async (req, res) => {
        const {id} = req.params;
        if(!id) {
            res.status(400).send({message: "Id not provided"});
        }
        CrocodilDb.destroy({
            where: {id}
        }).then(() => {
            res.status(200).send({message: "Crocodil Sters"});
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error"});
        });
    },
    //actualizarea numelui crocodilului
    updateCrocodile: async (req, res) => {
        const {nume, prenume} = req.body;
        const {id} = req.params;
        if(!id) {
            res.status(400).send({message: "Id not provided"});
        }
        CrocodilDb.update(
            {nume, prenume},
            {where: {id}}
        ).then (() => {
            res.status(200).send({message: "Crocodil actualizat"});
        }).catch((err) => {
            console.log(err);
            res.status(500).send({message: "Server error"});
        });
    }
};

module.exports = controller;
