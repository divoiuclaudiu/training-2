const Sequelize = require("sequelize");

const sequelize = new Sequelize("sequelize_training", "root", "", {
    dialect: "mysql",
    host: "localhost",
    define: {
        timestamp: true,
    }
});

module.exports = sequelize;