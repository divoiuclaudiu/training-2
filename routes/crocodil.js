const express = require('express');
const router = express.Router();
const crocodilController = require("../controllers").crocodil;


router.get("/", crocodilController.getAllCrocodiles);
router.post("/", crocodilController.addCrocodile);
router.delete("/:id", crocodilController.deleteCrocodile); //ruta pentru stergerea unui crocodil
router.put("/:id", crocodilController.updateCrocodile); //ruta pentru actualizarea unui crocodil

module.exports = router;