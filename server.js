const express = require('express');
const connection = require ("./models").connection;
const router = require("./routes");

const app = express();

let port = 8080;

app.use(express.json());

app.use("/api", router);

app.get("/reset", (req, res) => {
    connection.sync({force: true}).then(() => {
        res.status(201).send({message: "Database reset"});
    }).catch(() => {
        res.status(500).send({message: "Database reset failed"});
    });
});

app.use("/*", (req, res) => {
    res.status(200).send("App ruleaza");
});

app.listen(port, () => {
    console.log("Server is running on " + port);
});